// ---------------------------------------------------- 
// Author: 	Elisabete Baker
// Date: 	March 2017
// Project: Gulp file created for ecodes website
// ---------------------------------------------------- 

var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();

// compile scss to css
gulp.task('sass', function(){
	return gulp.src('app/scss/**/*.scss')	// find scss file here
	.pipe(sass())							// compile with gulp sass
	.pipe(gulp.dest('app/css'))				// save css file here
	.pipe(browserSync.reload ({				// reload page after scss change
		stream: true
	}))
});

// watch files for updates to autocompile, reload browser
gulp.task('watch', ['browserSync', 'sass'], function(){
	gulp.watch('app/scss/**/*.scss', ['sass']);
	gulp.watch('app/*.html', browserSync.reload);
	gulp.watch('app/js/**/*.js', browserSync.reload);
})


// reload browser upon file change
gulp.task('browserSync', function(){
	browserSync.init({
		server: {
			baseDir: 'app'
		},
	})
});

