Author:     Elisabete Baker

Date:       April 2017

Project:    Elisabete�s Portfolio Website

----

Howdy folks!

Thanks for checking out my git repo. This one (ecodes) is dedicated to [my current website](http://elisabete.codes/).

Since my other repo mostly consists of pdf work samples, I thought I'd give you all access to one with real code. ;)

Happy reading!
Elisabete